swiftFontAwesome
================

# How to use font awesome in swift

Font awesome can be a useful part of any project.  It comes with a set of common icons and image assets.  Since you use text instead of images, you can resize without pixellation and customize the color.

Common use cases include social media icons, button icons, camera and play button overlays, etc.  

Integrating font awesome is as simple as integrating any other font.

1. Create a new project
1. Download font awesome from http://fortawesome.github.io/Font-Awesome/
1. Unzip the folder and drag fonts/fontawesome-webfont.ttf into your project
1. Open Supporting Files/Info.plist
  1. Add a new key "Fonts provided by application".  This is of type Array.
  1. Expand the array, and for Item 0, set the string value to "fontawesome-webfont.ttf"

Now that you have integrated font awesome into your project, try to create an icon.

1. Drop a label on your storyboard
1. Set the font to "Custom", and the family to FontAwesome
1. Set the font size to 40, and the color to Aqua
1. Declare this in your view controller `@IBOutlet weak var fontAwesomeIcon: UILabel!` and wire it to the storyboard
1. Here's the important part.  In viewDidLoad, set the label's text to the following unicode:
```
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    fontAwesomeIcon.text = "\u{f082}"
  }
```

The only tricky part is setting the unicode of the text label programmatically.  For some reason, the unicode cannot be set in the storyboard nor the runtime attributes.  This may be due to the unicode syntax in swift.

