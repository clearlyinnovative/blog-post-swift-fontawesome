//
//  ViewController.swift
//  fontAwesome
//
//  Created by Robert Chen on 10/29/14.
//  Copyright (c) 2014 ClearlyInnovative. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fontAwesomeIcon: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        fontAwesomeIcon.text = "\u{f082}"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

